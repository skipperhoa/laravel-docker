## Cài đặt Docker & Docker Compose in Ubutu 20.04
[Link hướng dẫn](https://hoanguyenit.com/public/Note-Dev/install-docker/)

## Tích hợp adminer quản lý database
Thêm code sao vào file Dockerfile
adminer:
   image: adminer
   restart: always
   ports:
      - 8080:8080
   networks:
      - travellist

+ mở trình duyệt http://localhost:8080 có thể dăng nhập, tùy vào thông tin cung cấp user,pass, database mà ta cung cấp để đăng nhập
user: user
pass: user
database: mydb

## Đóng một container
# find container id running on the desired port from the following output
docker container ls
# stop the container running on the desired port
docker stop <container-id>

[https://hub.docker.com/_/adminer](https://hub.docker.com/_/adminer)

[https://github.com/Beachcasts/doctrine-expressive-example/blob/master/container-build/web/Dockerfile]

(https://github.com/Beachcasts/doctrine-expressive-example/blob/master/container-build/web/Dockerfile)

[How To Install and Set Up Laravel with Docker Compose on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-set-up-laravel-with-docker-compose-on-ubuntu-20-04)

[How To Set Up Laravel, Nginx, and MySQL With Docker Compose on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose-on-ubuntu-20-04)

[https://github.com/Beachcasts/doctrine-expressive-example/blob/master/docker-compose.yml](https://github.com/Beachcasts/doctrine-expressive-example/blob/master/docker-compose.yml)

[Setup Adminer with docker for Database Management](https://dev.to/codewithml/setup-adminer-with-docker-for-database-management-4dd2)

[https://github.com/minhnv2306/laravel-docker/blob/01cea0eb1af8007e6e32ba4c2803088ac98dd3ce/docker-compose.yml](https://github.com/minhnv2306/laravel-docker/blob/01cea0eb1af8007e6e32ba4c2803088ac98dd3ce/docker-compose.yml)

[https://gist.github.com/ralphschindler/8f2f17150ad4b9d3764c334733f9e3a0](https://gist.github.com/ralphschindler/8f2f17150ad4b9d3764c334733f9e3a0)
